console.log("Welcome to jQuery.");

  $(document).ready(function(){

    var c = $("#canvas");
    var ctx = c[0].getContext('2d');

    var start = -10;
    var end = 0;
    var radius = 5;

    while (start < 1800) {

       dibujaCenefa(ctx, start, end, radius);

       start = start + 20;
       end = 25
     }


    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.rect(0,0,20,30);
    ctx.rect(0,0,20,20);

    ctx.lineTo(18,5);
    ctx.lineTo(0,9.5);
    ctx.lineTo(18,15);
    ctx.lineTo(0,20);
    ctx.stroke();

    function dibujaCenefa(ctx, centerx, centery, radius ) {
      ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.arc(centerx, centery, radius, 0, 2 * Math.PI);
        ctx.stroke();
      }

});
